package com.eductate.course.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SearchResponseDto extends BaseDto{

	private static final long serialVersionUID = 1L;
	
	private List<CourseDto> courseLists;
	
	private List<CategoryDto> categoryLists;
}
