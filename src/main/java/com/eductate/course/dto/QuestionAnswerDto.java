package com.eductate.course.dto;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QuestionAnswerDto extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private String questionAnswerKey;
	
	private String question;
	
	private String answer;
	
	private StatusTypes status;
		
	private String courseKey;
	
	private String userKey;

}
