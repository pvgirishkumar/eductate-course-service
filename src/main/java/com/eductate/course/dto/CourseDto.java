package com.eductate.course.dto;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CourseDto extends BaseDto{
	
	private static final long serialVersionUID = 1L;
	
	private String courseKey;
	
	private String title;
	
	private String description;
	
	private String previewImg;
	
	private String previewVideo;
	
	private String userKey;
	
	private String teacherName;
	
	private double price;
	
	private StatusTypes status;
	
	private String categoryKey;
	
	private String categoryTitle;	
	
//	private Date startTime;
//	
//	private Date endTime;	
//	
//	private int clazzType;

}
