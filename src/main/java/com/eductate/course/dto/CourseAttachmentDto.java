package com.eductate.course.dto;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CourseAttachmentDto extends BaseDto{
	
	private static final long serialVersionUID = 1L;
	
	private String courseAttachmentKey;
	
	private String title;

	private String description;
	
	private String courseKey;
	
	private String attachmentKey;
	
	private StatusTypes status;

}
