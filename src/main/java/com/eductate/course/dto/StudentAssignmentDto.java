package com.eductate.course.dto;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StudentAssignmentDto extends BaseDto{
	
	private static final long serialVersionUID = 1L;
	
	private String studentAssignmentKey;	
	
	private StatusTypes status;
	
	private String assignmentKey;
	
	private String courseKey;
	
	private String userKey;
	
	private String answer;
	
	private String answerFileUrl;
	
	private double score;

}
