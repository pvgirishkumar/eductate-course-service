package com.eductate.course.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AttachmentDto extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private String attachmentKey;
	
	private String mediaUrl;

	private String mediaType;

}
