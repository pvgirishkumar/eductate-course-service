package com.eductate.course.dto;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AssignmentDto extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private String assignmentKey;
	
	private String title;

	private String description;
	
	private String assignmentFileUrl;
	
	private StatusTypes status;
	
	private String courseKey;
	
	private String userKey;

}
