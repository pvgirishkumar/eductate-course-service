package com.eductate.course.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eductate.course.model.Attachments;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachments, Long> {
	
	Optional<Attachments> findByAttachmentKey(String attachmentKey);

}
