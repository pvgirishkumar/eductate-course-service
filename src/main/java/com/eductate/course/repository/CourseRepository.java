package com.eductate.course.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.eductate.course.model.Course;


@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

	Optional<Course> findByCourseKey(String courseKey);
	
	List<Course> findAllCourseByUserKey(String userKey);
//	
//	List<Clazz> findAllClazzByBatchKey(String batchKey);
	
	@Query("select u from Course u where u.title LIKE %:title%  OR u.description LIKE %:description%")
	List<Course> findAllCategoryByTitleOrDescription(@Param("title") String title,@Param("description") String description);
	
}
