package com.eductate.course.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.eductate.course.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	Optional<Category> findByCategoryKey(String categoryKey);
	
	@Query("select u from Category u where u.title LIKE %:title%  OR u.description LIKE %:description%")
	List<Category> findAllCategoryByTitleOrDescription(@Param("title") String title,@Param("description") String description);

}
