package com.eductate.course.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eductate.course.model.StudentAssignment;

@Repository
public interface StudentAssignmentRepository extends JpaRepository<StudentAssignment, Long>{

	Optional<StudentAssignment> findByStudentAssignmentKey(String studentAssignmentKey);
}
