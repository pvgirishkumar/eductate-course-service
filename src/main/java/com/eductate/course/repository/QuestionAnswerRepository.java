package com.eductate.course.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eductate.course.model.QuestionAnswer;

@Repository
public interface QuestionAnswerRepository extends JpaRepository<QuestionAnswer, Long> {
	
	Optional<QuestionAnswer> findByQuestionAnswerKey(String questionAnswerKey);

}
