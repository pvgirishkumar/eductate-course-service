package com.eductate.course.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eductate.course.model.CourseAttachment;

@Repository
public interface CourseAttachmentRepository extends JpaRepository<CourseAttachment, Long> {

	Optional<CourseAttachment> findByCourseAttachmentKey(String courseAttachmentKey);
	
	List<CourseAttachment> findAllByCourseKey(String courseKey);
}
