package com.eductate.course.enums;

public enum StatusTypes {

	PENDINGAPPROVAL,BOOKED,APPROVED,DISAPPROVED,JOINED,COMPLETED,CANCELLED,DELETED;
	
	public static StatusTypes fromString (String value) {
		StatusTypes cpStatus = null;
		for(StatusTypes s:StatusTypes.values()) {
			if(s.name().equalsIgnoreCase(value)) {
				cpStatus = s;
				break;
			}
		}
		return cpStatus;
	}
}
