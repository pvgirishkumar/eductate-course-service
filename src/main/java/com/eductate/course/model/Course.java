package com.eductate.course.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
@Entity
@Table(name = "course")
public class Course extends BaseObject{
	
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "course_key", nullable = true, unique = true)
	private String courseKey;
	
	@Column(name = "title")
	private String title;
	
	@Lob
	@Column(name = "description")
	private String description;
	
	@Column(name = "preview_img")
	private String previewImg;
	
	@Column(name = "preview_video")
	private String previewVideo;
	
	@Column(name = "user_key", nullable = false)
	private String userKey;
	
	@Column(name = "teacher_name", nullable = true)
	private String teacherName;
	
	@Enumerated(EnumType.STRING)
	private StatusTypes status;
	
	@Column(name = "price") 
	private double price;
	
	@Column(name = "category_key", nullable = true) 
	private String categoryKey;
	
	@Column(name = "category_title", nullable = true) 
	private String categoryTitle;
	
	/*
	 * @Column(name = "start_time") private Date startTime;
	 * 
	 * @Column(name = "end_time") private Date endTime;
	 * 
	 * @Column(name = "status") private String status;
	 * 
	 * @Column(name = "course_type", nullable = false) private int courseType;
	 */

}
