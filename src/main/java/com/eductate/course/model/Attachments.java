package com.eductate.course.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "attachments")
public class Attachments extends BaseObject {

	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "attachment_key", nullable = true, unique = true)
	private String attachmentKey;
	
	@Column(name = "media_url")
	private String mediaUrl;

	@Column(name = "media_type")
	private String mediaType;
}
