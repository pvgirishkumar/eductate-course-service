package com.eductate.course.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;

import com.eductate.course.utils.EduConstants;

import lombok.Getter;
import lombok.Setter;


@MappedSuperclass
@Getter
@Setter
public class BaseObject implements Serializable, EduConstants{
	
	private static final long serialVersionUID = APP_SERIAL_ID;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@CreationTimestamp
	private Date lastUpdatedOn;
	
	@CreationTimestamp
	private Date createdOn;

}