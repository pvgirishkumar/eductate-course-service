package com.eductate.course.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "student_assignments")
public class StudentAssignment extends BaseObject {
	
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "student_assignment_key", nullable = true, unique = true)
	private String studentAssignmentKey;	
	
	@Enumerated(EnumType.STRING)
	private StatusTypes status;
	
	@Column(name = "assignment_key", nullable = false)
	private String assignmentKey;
	
	@Column(name = "course_key", nullable = false)
	private String courseKey;
	
	@Column(name = "user_key", nullable = false)
	private String userKey;
	
	@Lob
	@Column(name = "answer")
	private String answer;
	
	@Column(name = "answer_file")
	private String answerFileUrl;
	
	@Column(name = "score") 
	private double score;
	
	

}
