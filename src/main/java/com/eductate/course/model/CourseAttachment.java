package com.eductate.course.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "course_attachments")
public class CourseAttachment extends BaseObject {
	
private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "course_attachment_key", nullable = true, unique = true)
	private String courseAttachmentKey;
	
	@Column(name = "title")
	private String title;

	@Lob
	@Column(name = "description")
	private String description;
	
	@Column(name = "course_key", nullable = false)
	private String courseKey;
	
	@Column(name = "attachment_key", nullable = false)
	private String attachmentKey;
	
	@Enumerated(EnumType.STRING)
	private StatusTypes status;

}
