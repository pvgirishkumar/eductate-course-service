package com.eductate.course.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "assignments")
public class Assignment extends BaseObject {
	
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "assignment_key", nullable = true, unique = true)
	private String assignmentKey;
	
	@Column(name = "title")
	private String title;

	@Lob
	@Column(name = "description")
	private String description;
	
	@Column(name = "assignment_file")
	private String assignmentFileUrl;
	
	@Enumerated(EnumType.STRING)
	private StatusTypes status;
	
	@Column(name = "course_key", nullable = false)
	private String courseKey;
	
	@Column(name = "user_key", nullable = false)
	private String userKey;

}
