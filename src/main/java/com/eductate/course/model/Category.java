package com.eductate.course.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "categories")
public class Category extends BaseObject{
	
	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "category_key", nullable = true, unique = true)
	private String categoryKey;
	
	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;
	
	@Enumerated(EnumType.STRING)
	private StatusTypes status;

}
