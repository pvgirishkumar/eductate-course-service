package com.eductate.course.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.eductate.course.enums.StatusTypes;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "question_answers")
public class QuestionAnswer extends BaseObject {

	private static final long serialVersionUID = APP_SERIAL_ID;
	
	@Column(name = "question_answer_key", nullable = true, unique = true)
	private String questionAnswerKey;
	
	@Lob
	@Column(name = "question")
	private String question;
	
	@Lob
	@Column(name = "answer")
	private String answer;
	
	@Enumerated(EnumType.STRING)
	private StatusTypes status;
		
	@Column(name = "course_key", nullable = false)
	private String courseKey;
	
	@Column(name = "user_key", nullable = false)
	private String userKey;
}
