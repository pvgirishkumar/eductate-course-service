package com.eductate.course;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication(
		exclude = {
	            org.springframework.cloud.aws.autoconfigure.context.ContextInstanceDataAutoConfiguration.class,
	            org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration.class,
	            org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration.class
	    }
	)
public class EductateCourseServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EductateCourseServiceApplication.class, args);
	}
	
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilter() {
	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    
	    CorsConfiguration configAutenticacao = new CorsConfiguration();
	    configAutenticacao.setAllowCredentials(true);
	    configAutenticacao.addAllowedOrigin("http://localhost:3000");
	    configAutenticacao.addAllowedOrigin("http://localhost:8888");
		configAutenticacao.addAllowedOrigin("http://localhost");
	    configAutenticacao.addAllowedMethod("*");
	    configAutenticacao.addAllowedHeader("*");
	    configAutenticacao.setMaxAge(3600L);
	     source.registerCorsConfiguration("/**", configAutenticacao);
	    
	    FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
	    bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
	    return bean;
	}

}
