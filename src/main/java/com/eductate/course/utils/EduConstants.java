package com.eductate.course.utils;

public interface EduConstants {
		
	long APP_SERIAL_ID = 1L;

	int ERROR_CODE_UNKOWN = 0;

	int SUCCESS_CODE = 1000;

	int ERROR_CODE = 1004;

	String SUCCESS = "Success";

	String EMAIL = "email";

}
