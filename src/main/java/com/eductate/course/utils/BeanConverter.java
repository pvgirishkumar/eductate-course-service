package com.eductate.course.utils;

import com.eductate.course.dto.AssignmentDto;
import com.eductate.course.dto.AttachmentDto;
import com.eductate.course.dto.BaseDto;
import com.eductate.course.dto.CategoryDto;
import com.eductate.course.dto.CourseAttachmentDto;
import com.eductate.course.dto.CourseDto;
import com.eductate.course.dto.QuestionAnswerDto;
import com.eductate.course.dto.StudentAssignmentDto;
import com.eductate.course.model.Assignment;
import com.eductate.course.model.Attachments;
import com.eductate.course.model.BaseObject;
import com.eductate.course.model.Category;
import com.eductate.course.model.Course;
import com.eductate.course.model.CourseAttachment;
import com.eductate.course.model.QuestionAnswer;
import com.eductate.course.model.StudentAssignment;

public class BeanConverter {

	private BeanConverter() {

	}
	
	private static void toBaseDto(BaseObject source, BaseDto target) {
		if(source != null && target != null) {
			target.setId(source.getId());
			target.setLastUpdatedOn(source.getLastUpdatedOn());
			target.setCreatedOn(source.getCreatedOn());
		}
	}
	
	public static CourseDto toCourseDto(Course source) {
		CourseDto target = null;
		if(source != null) {
			target = new CourseDto();
			toBaseDto(source, target);
			target.setCourseKey(source.getCourseKey());
			target.setTitle(source.getTitle());
			target.setDescription(source.getDescription());
			target.setPreviewImg(source.getPreviewImg());
			target.setPreviewVideo(source.getPreviewVideo());
			target.setUserKey(source.getUserKey());
			target.setTeacherName(source.getTeacherName());
			target.setPrice(source.getPrice());
			target.setStatus(source.getStatus());
			target.setCategoryKey(source.getCategoryKey());
			target.setCategoryTitle(source.getCategoryTitle());
			
			/*
			 * target.setStartTime(source.getStartTime());
			 * target.setEndTime(source.getEndTime());
			 * target.setCourseType(source.getCourseType());
			 */
		}
		return target;
	}
	
	public static CategoryDto toCategoryDto(Category source) {
		CategoryDto target = null;
		if(source != null) {
			target =  new CategoryDto();
			toBaseDto(source, target);
			target.setCategoryKey(source.getCategoryKey());
			target.setDescription(source.getDescription());
			target.setTitle(source.getTitle());
			target.setStatus(source.getStatus());
		}
		return target;
	}
	
	public static AttachmentDto toAttachmentDto(Attachments source) {
		AttachmentDto target = null;
		if(source != null) {
			target = new AttachmentDto();
			toBaseDto(source, target);
			target.setAttachmentKey(source.getAttachmentKey());
			target.setMediaUrl(source.getMediaUrl());
			target.setMediaType(source.getMediaType());
		}
		return target;
	}
	
	public static CourseAttachmentDto toCourseAttachmentDto(CourseAttachment source) {
		CourseAttachmentDto target = null;
		if(source != null) {
			target = new CourseAttachmentDto();
			toBaseDto(source, target);
			target.setTitle(source.getTitle());
			target.setDescription(source.getDescription());
			target.setStatus(source.getStatus());
			target.setCourseKey(source.getCourseKey());
			target.setAttachmentKey(source.getAttachmentKey());
		}
		return target;
	}
	
	public static AssignmentDto toAssignmentDto(Assignment source) {
		AssignmentDto target = null;
		if(source != null) {
			target = new AssignmentDto();
			toBaseDto(source, target);
			target.setAssignmentKey(source.getAssignmentKey());
			target.setTitle(source.getTitle());
			target.setDescription(source.getDescription());
			target.setAssignmentFileUrl(source.getAssignmentFileUrl());
			target.setStatus(source.getStatus());
			target.setCourseKey(source.getCourseKey());
			target.setUserKey(source.getUserKey());
		}
		return target;
	}
	
	public static StudentAssignmentDto toStudentAssignmentDto(StudentAssignment source) {
		StudentAssignmentDto target = null;
		if(source != null) {
			target = new StudentAssignmentDto();
			toBaseDto(source, target);
			target.setStudentAssignmentKey(source.getStudentAssignmentKey());
			target.setAssignmentKey(source.getAssignmentKey());
			target.setStatus(source.getStatus());
			target.setCourseKey(source.getCourseKey());
			target.setUserKey(source.getUserKey());
			target.setAnswer(source.getAnswer());
			target.setAnswerFileUrl(source.getAnswerFileUrl());
			target.setScore(source.getScore());
		}
		return target;
	}
	
	public static QuestionAnswerDto toQuestionAnswerDto(QuestionAnswer source) {
		QuestionAnswerDto target = null;
		if(source != null) {
			target = new QuestionAnswerDto();
			toBaseDto(source, target);
			target.setQuestionAnswerKey(source.getQuestionAnswerKey());
			target.setStatus(source.getStatus());
			target.setCourseKey(source.getCourseKey());
			target.setUserKey(source.getUserKey());
			target.setQuestion(source.getQuestion());
			target.setAnswer(source.getAnswer());
		}
		return target;
	}
	
}
