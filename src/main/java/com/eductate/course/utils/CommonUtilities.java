package com.eductate.course.utils;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;

public class CommonUtilities {
	private static final String ALPHABETS = "CXDEHIAYZBJKLMPQRSFGTUVWNO";

	private static final String NUMERICS = "8261495730";

	public static String getUniqueId(int count, Long id) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHABETS.length());
			builder.append(ALPHABETS.charAt(character));
		}

		if (id != null) {
			builder.append(id);
		}
		return builder.toString();
	}
	
	public static String getFormattedDate(Date date, String valueStr) {
		if(valueStr != null) {
			return valueStr;
		}
		String value = "";
		if(date != null) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
				return formatter.format(date);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return value;
	}
	
	public static String getFormattedTime(Date date, String valueStr) {
		if(valueStr != null) {
			return valueStr;
		}
		String value = "";
		if(date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("hh.mm aa");
			return formatter.format(date);
		}
		return value;
	}
	
}
