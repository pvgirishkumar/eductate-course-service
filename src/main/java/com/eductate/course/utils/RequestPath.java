package com.eductate.course.utils;

public interface RequestPath {
	
	public static final String V1 = "v1/";
	
//	public static final String ADMIN = "admin/";
//	
//	public static final String SYLLABUS = "syllabus";
//
//	public static final String TEACHER = "teacher";
//	
//	public static final String BATCH = "batch";
//	
//	public static final String STUDENT = "student";
	
//	public static final String CLAZZ = "clazz";

	public static final String COURSE = "course";
//	
//	public static final String INSTITUTE = "institute";
//	
//	public static final String CHAPTER = "chapter";
//	
//	public static final String CONTENT = "content";
//	
//	public static final String USER_PROFILE = "user-profile";
	
	public static final String CATEGORY = "category";
	
//	public static final String CONNECT_CLAZZ = "connect-clazz";
//	
//	public static final String SUBJECT = "subject";
//	
//	public static final String UNIT = "unit";
//	
	public static final String ASSIGNMENT = "assignment";
	
	public static final String STUDENT_ASSIGNMENT = "student-assignment";
	
	public static final String QUESTION_ANSWERS = "question-answer";
	
//	public static final String ASSIGNMENT_CONTENT = "assignment-content";
	
	public static final String ATTACHMENT = "attachment";
	
	public static final String COURSE_ATTACHMENT = "course-attachment";

}
