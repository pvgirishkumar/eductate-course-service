package com.eductate.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.course.dto.StudentAssignmentDto;
import com.eductate.course.service.IStudentAssignmentService;
import com.eductate.course.utils.RequestPath;

@RestController
@RequestMapping(RequestPath.V1 + RequestPath.STUDENT_ASSIGNMENT)
public class StudentAssignmentController {
	
	@Autowired
	private IStudentAssignmentService studentAssignService;
	
	@PostMapping
	public StudentAssignmentDto createStudentAssignment(@RequestBody StudentAssignmentDto source) {
		return studentAssignService.createStudentAssignment(source);
	}
	
	@PutMapping
	public StudentAssignmentDto updateAssignment(@RequestBody StudentAssignmentDto source) {
		return studentAssignService.updateStudentAssignment(source);
	}
	
	@GetMapping("{studentAssignmentKey}")
	public StudentAssignmentDto getAssignment(@PathVariable(name="studentAssignmentKey") String studentAssignmentKey) {
		return studentAssignService.getStudentAssignment(studentAssignmentKey);
	}

}
