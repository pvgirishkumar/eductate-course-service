package com.eductate.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.course.dto.AssignmentDto;
import com.eductate.course.service.IAssignmentService;
import com.eductate.course.utils.RequestPath;

@RestController
@RequestMapping(RequestPath.V1 + RequestPath.ASSIGNMENT)
public class AssignmentController {
	
	@Autowired
	private IAssignmentService assignService;
	
	@PostMapping
	public AssignmentDto createAssignment(@RequestBody AssignmentDto source) {
		return assignService.createAssignment(source);
	}
	
	@PutMapping
	public AssignmentDto updateAssignment(@RequestBody AssignmentDto source) {
		return assignService.updateAssignment(source);
	}
	
	@GetMapping("{assignmentKey}")
	public AssignmentDto getAssignment(@PathVariable(name="assignmentKey") String assignmentKey) {
		return assignService.getAssignment(assignmentKey);
	}

}
