package com.eductate.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.eductate.course.dto.AttachmentDto;
import com.eductate.course.service.IAttachmentService;
import com.eductate.course.utils.RequestPath;

@RestController
@RequestMapping(RequestPath.V1 + RequestPath.ATTACHMENT)
public class AttachmentController {
	
	@Autowired
	private IAttachmentService attachService;
	
	@PostMapping(consumes = {MediaType.ALL_VALUE} )
	public AttachmentDto createAttachment(@RequestParam(value="file", required=true) MultipartFile mediaFile) {
		return attachService.createAttachment(mediaFile);
	}
	
	@GetMapping("{attachmentKey}")
	public AttachmentDto getAttachment(@PathVariable(name="attachmentKey") String attachmentKey) {
		return attachService.getAttachment(attachmentKey);
	}
}
