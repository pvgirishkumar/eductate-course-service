package com.eductate.course.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.course.dto.CategoryDto;
import com.eductate.course.service.ICategoryService;
import com.eductate.course.utils.RequestPath;

@RestController
@RequestMapping(RequestPath.V1 + RequestPath.CATEGORY)
public class CategoryController {
	
	@Autowired
	private ICategoryService categoryService;

	@PostMapping
	public CategoryDto createCategory(@RequestBody CategoryDto source) {
		return categoryService.createCategory(source);
	}
	
	@PutMapping
	public CategoryDto updateCategory(@RequestBody CategoryDto source) {
		return categoryService.createCategory(source);
	}
	
	@GetMapping("{categoryKey}")
	public CategoryDto getCategory(@PathVariable(name="categoryKey") String categoryKey) {
		return categoryService.getCategory(categoryKey);
	}
	
	@GetMapping
	public List<CategoryDto> getAllCategory() {
		return categoryService.getAllCategory();
	}
	
	
}
