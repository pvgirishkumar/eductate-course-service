package com.eductate.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.course.dto.QuestionAnswerDto;
import com.eductate.course.service.IQuestionAnswerService;
import com.eductate.course.utils.RequestPath;

@RestController
@RequestMapping(RequestPath.V1 + RequestPath.QUESTION_ANSWERS)
public class QuestionAnswerController {

	@Autowired
	private IQuestionAnswerService quesAnsService;
	
	@PostMapping
	public QuestionAnswerDto createQuestionAnswer(@RequestBody QuestionAnswerDto source) {
		return quesAnsService.createQuestionAnswer(source);
	}
	
	@PutMapping
	public QuestionAnswerDto updateQuestionAnswer(@RequestBody QuestionAnswerDto source) {
		return quesAnsService.updateQuestionAnswer(source);
	}
	
	@GetMapping("{questionAnswerKey}")
	public QuestionAnswerDto getQuestionAnswer(@PathVariable(name="questionAnswerKey") String questionAnswerKey) {
		return quesAnsService.getQuestionAnswer(questionAnswerKey);
	}
}
