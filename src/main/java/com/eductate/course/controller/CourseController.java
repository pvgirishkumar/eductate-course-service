package com.eductate.course.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.course.dto.CourseDto;
import com.eductate.course.dto.SearchResponseDto;
import com.eductate.course.service.ICategoryService;
import com.eductate.course.service.ICourseService;
import com.eductate.course.utils.RequestPath;


@RestController
@RequestMapping(RequestPath.V1 + RequestPath.COURSE)
public class CourseController {

	@Autowired
	private ICourseService courseService;
	
	@Autowired
	private ICategoryService cateService;
	
	@PostMapping
	public CourseDto createCourse(@RequestBody CourseDto source) {
		return courseService.createCourse(source);
	}
	
	@PutMapping
	public CourseDto updateCourse(@RequestBody CourseDto course) {
		return courseService.updateCourse(course);
	}
	
	@GetMapping("{courseKey}")
	public CourseDto getCourse(@PathVariable(name="courseKey") String courseKey) {
		return courseService.getCourse(courseKey);
	}
	
	@GetMapping("tutor/"+"{userKey}")
	public List<CourseDto> getAllCourseByUser(@PathVariable(name="userKey") String userKey) {
		return courseService.findAllCourseByUser(userKey);
	}
	
	@GetMapping("all")
	public List<CourseDto> getAllCourses() {
		return courseService.findAllCourses();
	}
	
	@GetMapping("search/"+"{searchKeyword}")
	public SearchResponseDto searchAllByTitleOrDescription(@PathVariable(name="searchKeyword") String searchKeyword) {
		SearchResponseDto result = new SearchResponseDto();
		result.setCourseLists(courseService.findAllCoursesByTitleOrDescription(searchKeyword));
		result.setCategoryLists(cateService.findAllCategoryByTitleOrDescription(searchKeyword));
		return result;
		//return courseService.findAllCoursesByTitleOrDescription(searchKeyword, searchKeyword);
	}
}
