package com.eductate.course.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eductate.course.dto.CategoryDto;
import com.eductate.course.dto.CourseAttachmentDto;
import com.eductate.course.service.ICourseAttachmentService;
import com.eductate.course.utils.RequestPath;

@RestController
@RequestMapping(RequestPath.V1 + RequestPath.COURSE_ATTACHMENT)
public class CourseAttachmentController {
	
	@Autowired
	private ICourseAttachmentService courseAttachService;
	
	@PostMapping
	public CourseAttachmentDto createCourseAttachment(@RequestBody CourseAttachmentDto source) {
		return courseAttachService.createCourseAttachment(source);
	}
	
	@PutMapping
	public CourseAttachmentDto updateCourseAttachment(@RequestBody CourseAttachmentDto source) {
		return courseAttachService.updateCourseAttachment(source);
	}
	
	@GetMapping("{courseAttachmentKey}")
	public CourseAttachmentDto getCourseAttachment(@PathVariable(name="courseAttachmentKey") String courseAttachmentKey) {
		return courseAttachService.getCourseAttachment(courseAttachmentKey);
	}
	
	@GetMapping("all/{courseAttachmentKey}")
	public List<CourseAttachmentDto> getAllCourseAttachmentByCourse(@PathVariable(name="courseAttachmentKey") String courseAttachmentKey) {
		return courseAttachService.getAllCourseAttachment(courseAttachmentKey);
	}

}
