package com.eductate.course.service;

import com.eductate.course.dto.AssignmentDto;

public interface IAssignmentService {

	AssignmentDto createAssignment(AssignmentDto source);
	
	AssignmentDto updateAssignment(AssignmentDto source);
		
	AssignmentDto getAssignment(String assignmentKey);
}
