package com.eductate.course.service;

import java.util.List;

import com.eductate.course.dto.BaseDto;
import com.eductate.course.dto.CourseDto;

public interface ICourseService {

	CourseDto createCourse(CourseDto source);
	
	CourseDto updateCourse(CourseDto source);
	
	CourseDto getCourse(String courseKey);
	
	BaseDto deleteCourse(Long id);
	
	List<CourseDto> findAllCourseByUser(String userKey);
	
	List<CourseDto> findAllCourses();
	
	List<CourseDto> findAllCoursesByTitleOrDescription(String searchKeyword);
}
