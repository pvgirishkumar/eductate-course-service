package com.eductate.course.service;

import java.util.List;

import com.eductate.course.dto.CategoryDto;

public interface ICategoryService {

	CategoryDto createCategory(CategoryDto cateDto);
	
	CategoryDto updateCategory(CategoryDto cateDto);
	
	CategoryDto getCategory(String categoryKey);
	
	List<CategoryDto> getAllCategory();
	
	List<CategoryDto> findAllCategoryByTitleOrDescription(String searchKeyword);
	
}
