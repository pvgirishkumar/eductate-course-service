package com.eductate.course.service;

import java.util.List;

import com.eductate.course.dto.CourseAttachmentDto;

public interface ICourseAttachmentService {
	
	CourseAttachmentDto createCourseAttachment(CourseAttachmentDto source);
	
	CourseAttachmentDto updateCourseAttachment(CourseAttachmentDto source);
	
	CourseAttachmentDto getCourseAttachment(String courseAttachmentKey);
	
	List<CourseAttachmentDto> getAllCourseAttachment(String courseKey);

}
