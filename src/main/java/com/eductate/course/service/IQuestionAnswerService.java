package com.eductate.course.service;

import com.eductate.course.dto.QuestionAnswerDto;

public interface IQuestionAnswerService {
	
	QuestionAnswerDto createQuestionAnswer(QuestionAnswerDto source);
	
	QuestionAnswerDto updateQuestionAnswer(QuestionAnswerDto source);
		
	QuestionAnswerDto getQuestionAnswer(String questionAnswerKey);

}
