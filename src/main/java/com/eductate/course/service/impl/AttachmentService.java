package com.eductate.course.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.eductate.course.dto.AttachmentDto;
import com.eductate.course.model.Attachments;
import com.eductate.course.repository.AttachmentRepository;
import com.eductate.course.service.IAttachmentService;
import com.eductate.course.utils.BeanConverter;
import com.eductate.course.utils.CommonUtilities;

@Service
@Transactional(readOnly = true)
public class AttachmentService implements IAttachmentService{
	
	@Autowired
	private AWSService awsService;
	
	@Autowired
	private AttachmentRepository attachRepo;

	@Override
	@Transactional
	public AttachmentDto createAttachment(MultipartFile mediaFile) {
		Attachments entity = new Attachments();
		AttachmentDto attachDto = new AttachmentDto();
		if(mediaFile != null) {
			String uploadedFileName = awsService.uploadToS3(mediaFile);
			if(uploadedFileName != null) {
				entity.setMediaUrl(uploadedFileName);
				entity.setMediaType(mediaFile.getContentType());
				attachRepo.save(entity);
				entity.setAttachmentKey(CommonUtilities.getUniqueId(16, entity.getId()));
				attachDto = BeanConverter.toAttachmentDto(entity);
			}
		}
		return attachDto;
	}

//	@Override
//	public AttachmentDto updateAttachment(AttachmentDto cateDto) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public AttachmentDto getAttachment(String attachmentKey) {
		AttachmentDto target = null;
		Optional<Attachments> entity =  attachRepo.findByAttachmentKey(attachmentKey);
		if(entity.isPresent()) {
			target = new AttachmentDto();
			BeanUtils.copyProperties(entity.get(), target);
		}
		return target;
	}

	@Override
	public List<AttachmentDto> getAllAttachment() {
		List<Attachments> lists = attachRepo.findAll();
		if(lists.size() > 0) {
			List<AttachmentDto> results = lists.stream().map(m -> BeanConverter.toAttachmentDto(m)).collect(Collectors.toList());
			return results;
		}
		return null;
	}
	
	

}
