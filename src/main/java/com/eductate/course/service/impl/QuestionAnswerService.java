package com.eductate.course.service.impl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.course.dto.QuestionAnswerDto;
import com.eductate.course.model.QuestionAnswer;
import com.eductate.course.repository.QuestionAnswerRepository;
import com.eductate.course.service.IQuestionAnswerService;
import com.eductate.course.utils.BeanConverter;
import com.eductate.course.utils.CommonUtilities;

@Service
@Transactional(readOnly = true)
public class QuestionAnswerService implements IQuestionAnswerService {
	
	@Autowired
	private QuestionAnswerRepository quesAnsRepo;

	@Override
	@Transactional
	public QuestionAnswerDto createQuestionAnswer(QuestionAnswerDto source) {
		QuestionAnswer entity = new QuestionAnswer();
		BeanUtils.copyProperties(source, entity);
		quesAnsRepo.save(entity);
		entity.setQuestionAnswerKey(CommonUtilities.getUniqueId(16, entity.getId()));
		source.setQuestionAnswerKey(entity.getQuestionAnswerKey());
		return source;
	}

	@Override
	@Transactional
	public QuestionAnswerDto updateQuestionAnswer(QuestionAnswerDto source) {
		Optional<QuestionAnswer> entity = quesAnsRepo.findByQuestionAnswerKey(source.getQuestionAnswerKey());
		if(entity.isPresent()) {
			QuestionAnswer target = entity.get();
			BeanUtils.copyProperties(source, target, "id");
			quesAnsRepo.save(target);
		}
		return source;
	}

	@Override
	public QuestionAnswerDto getQuestionAnswer(String questionAnswerKey) {
		Optional<QuestionAnswer> entity = quesAnsRepo.findByQuestionAnswerKey(questionAnswerKey);
		QuestionAnswer target = null;
		if(entity.isPresent()) {
			target = entity.get();
		}
		return BeanConverter.toQuestionAnswerDto(target);
	}

}
