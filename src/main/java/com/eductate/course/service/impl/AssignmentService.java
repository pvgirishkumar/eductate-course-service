package com.eductate.course.service.impl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.course.dto.AssignmentDto;
import com.eductate.course.model.Assignment;
import com.eductate.course.repository.AssignmentRepository;
import com.eductate.course.service.IAssignmentService;
import com.eductate.course.utils.BeanConverter;
import com.eductate.course.utils.CommonUtilities;

@Service
@Transactional(readOnly = true)
public class AssignmentService implements IAssignmentService {
	
	@Autowired
	private AssignmentRepository assignRepo;
	
	@Override
	@Transactional
	public AssignmentDto createAssignment(AssignmentDto source) {
		Assignment entity = new Assignment();
		BeanUtils.copyProperties(source, entity);
		assignRepo.save(entity);
		entity.setAssignmentKey(CommonUtilities.getUniqueId(16, entity.getId()));
		source.setAssignmentKey(entity.getAssignmentKey());
		return source;
	}

	@Override
	@Transactional
	public AssignmentDto updateAssignment(AssignmentDto source) {
		Optional<Assignment> entity = assignRepo.findByAssignmentKey(source.getAssignmentKey());
		if(entity.isPresent()) {
			Assignment target = entity.get();
			BeanUtils.copyProperties(source, target, "id");
			assignRepo.save(target);
		}
		return source;
	}

	@Override
	public AssignmentDto getAssignment(String assignmentKey) {
		Optional<Assignment> entity = assignRepo.findByAssignmentKey(assignmentKey);
		Assignment target = null;
		if(entity.isPresent()) {
			target = entity.get();
		}
		return BeanConverter.toAssignmentDto(target);
	}
}
