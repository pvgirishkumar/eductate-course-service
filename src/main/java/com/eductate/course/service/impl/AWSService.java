package com.eductate.course.service.impl;

import java.io.File;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

@Service
public class AWSService {

	@Value("${amazon.s3.bucket-name}")
    private String bucketName;
	
	@Value("${amazons3.access.url}")
    private String awsAccessURL;
	
	@Autowired
	private AmazonS3 amazonS3;
	
	public String uploadToS3(MultipartFile file) {
		String uploadedS3Url="Upload Failed";
		 if(file != null && file.getSize() > 0) {
			 try {
				 String fileName =  UUID.randomUUID().toString() +file.getOriginalFilename();
				 File tempFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
			     file.transferTo(tempFile);
			     PutObjectResult result = amazonS3.putObject(new PutObjectRequest(bucketName, fileName, tempFile)
			             .withCannedAcl(CannedAccessControlList.PublicRead));
			    		 
			     if(result != null){
			    	 return awsAccessURL+fileName;
			     }
			     tempFile.deleteOnExit();
			} catch (Exception e) {
				return uploadedS3Url;
			}
		 }
		return uploadedS3Url;
	}
	
	public String deleteFromS3(String fileName) {
		amazonS3.deleteObject(bucketName, fileName);
		return "deleted";
	}
}
