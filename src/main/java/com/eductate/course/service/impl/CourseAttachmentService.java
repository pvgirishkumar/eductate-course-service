package com.eductate.course.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.course.dto.CourseAttachmentDto;
import com.eductate.course.model.CourseAttachment;
import com.eductate.course.repository.CourseAttachmentRepository;
import com.eductate.course.service.ICourseAttachmentService;
import com.eductate.course.utils.BeanConverter;
import com.eductate.course.utils.CommonUtilities;

@Service
@Transactional(readOnly = true)
public class CourseAttachmentService implements ICourseAttachmentService{
	
	@Autowired
	private CourseAttachmentRepository courseAttachRepo;

	@Override
	@Transactional
	public CourseAttachmentDto createCourseAttachment(CourseAttachmentDto source) {
		CourseAttachment entity = new CourseAttachment();
		BeanUtils.copyProperties(source, entity);
		courseAttachRepo.save(entity);
		entity.setCourseAttachmentKey(CommonUtilities.getUniqueId(16, entity.getId()));
		source.setCourseAttachmentKey(entity.getCourseAttachmentKey());
		return source;
	}

	@Override
	@Transactional
	public CourseAttachmentDto updateCourseAttachment(CourseAttachmentDto source) {
		Optional<CourseAttachment> entity = courseAttachRepo.findByCourseAttachmentKey(source.getCourseAttachmentKey());
		if(entity.isPresent()) {
			CourseAttachment target = entity.get();
			BeanUtils.copyProperties(source, target, "id");
			courseAttachRepo.save(target);
		}
		return source;
	}

	@Override
	public CourseAttachmentDto getCourseAttachment(String courseAttachmentKey) {
		CourseAttachmentDto target = null;
		Optional<CourseAttachment> entity = courseAttachRepo.findByCourseAttachmentKey(courseAttachmentKey);
		if(entity.isPresent()) {
			target = new CourseAttachmentDto();
			BeanUtils.copyProperties(entity.get(), target);
		}
		return target;
	}

	@Override
	public List<CourseAttachmentDto> getAllCourseAttachment(String courseKey) {
		List<CourseAttachment> lists = courseAttachRepo.findAllByCourseKey(courseKey);
		if(lists.size() > 0) {
			List<CourseAttachmentDto> results = lists.stream().map(m -> BeanConverter.toCourseAttachmentDto(m)).collect(Collectors.toList());
			return results;
		}
		return null;
	}

	
}
