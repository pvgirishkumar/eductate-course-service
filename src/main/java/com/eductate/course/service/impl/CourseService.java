package com.eductate.course.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.course.dto.BaseDto;
import com.eductate.course.dto.CourseDto;
import com.eductate.course.model.Course;
import com.eductate.course.repository.CourseRepository;
import com.eductate.course.service.ICourseService;
import com.eductate.course.utils.BeanConverter;
import com.eductate.course.utils.CommonUtilities;


@Service
@Transactional(readOnly = true)
public class CourseService implements ICourseService{
	
	@Autowired
	private CourseRepository courseRepo;

	@Override
	@Transactional
	public CourseDto createCourse(CourseDto source) {
		Course entity = new Course();
		BeanUtils.copyProperties(source, entity);
		courseRepo.save(entity);
		entity.setCourseKey(CommonUtilities.getUniqueId(16, entity.getId()));
		source.setCourseKey(entity.getCourseKey());
		return source;
	}

	@Override
	@Transactional
	public CourseDto updateCourse(CourseDto source) {
		Optional<Course> entity = courseRepo.findByCourseKey(source.getCourseKey());
		if(entity.isPresent()) {
			Course target = entity.get();
			BeanUtils.copyProperties(source, target, "id");
			courseRepo.save(target);
		}
		return source;
	}

	@Override
	public CourseDto getCourse(String courseKey) {
		Optional<Course> entity = courseRepo.findByCourseKey(courseKey);
		Course target = null;
		if(entity.isPresent()) {
			target = entity.get();
		}
		return BeanConverter.toCourseDto(target);
	}

	@Override
	public BaseDto deleteCourse(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CourseDto> findAllCourseByUser(String userKey) {
		List<Course> lists = courseRepo.findAllCourseByUserKey(userKey);
		if(lists.size() > 0) {
			List<CourseDto> results = lists.stream().map(m -> BeanConverter.toCourseDto(m)).collect(Collectors.toList());
			return results;
		}
		return null;
	}
	
	@Override
	public List<CourseDto> findAllCourses() {
		List<Course> lists = courseRepo.findAll();
		if(lists.size() > 0) {
			List<CourseDto> results = lists.stream().map(m -> BeanConverter.toCourseDto(m)).collect(Collectors.toList());
			return results;
		}
		return null;
	}

	@Override
	public List<CourseDto> findAllCoursesByTitleOrDescription(String searchKeyword) {
		List<Course> lists = courseRepo.findAllCategoryByTitleOrDescription(searchKeyword, searchKeyword);
		if(lists.size() > 0) {
			List<CourseDto> results = lists.stream().map(m -> BeanConverter.toCourseDto(m)).collect(Collectors.toList());
			return results;
		}
		return null;
	}
	
}
