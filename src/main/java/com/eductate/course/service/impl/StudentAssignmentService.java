package com.eductate.course.service.impl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eductate.course.dto.StudentAssignmentDto;
import com.eductate.course.model.StudentAssignment;
import com.eductate.course.repository.StudentAssignmentRepository;
import com.eductate.course.service.IStudentAssignmentService;
import com.eductate.course.utils.BeanConverter;
import com.eductate.course.utils.CommonUtilities;

@Service
@Transactional(readOnly = true)
public class StudentAssignmentService implements IStudentAssignmentService {
	
	@Autowired
	private StudentAssignmentRepository studAssignRepo;

	@Override
	@Transactional
	public StudentAssignmentDto createStudentAssignment(StudentAssignmentDto source) {
		StudentAssignment entity = new StudentAssignment();
		BeanUtils.copyProperties(source, entity);
		studAssignRepo.save(entity);
		entity.setStudentAssignmentKey(CommonUtilities.getUniqueId(16, entity.getId()));
		source.setStudentAssignmentKey(entity.getStudentAssignmentKey());
		return source;
	}

	@Override
	@Transactional
	public StudentAssignmentDto updateStudentAssignment(StudentAssignmentDto source) {
		Optional<StudentAssignment> entity = studAssignRepo.findByStudentAssignmentKey(source.getStudentAssignmentKey());
		if(entity.isPresent()) {
			StudentAssignment target = entity.get();
			BeanUtils.copyProperties(source, target, "id");
			studAssignRepo.save(target);
		}
		return source;
	}

	@Override
	public StudentAssignmentDto getStudentAssignment(String studentAssignmentKey) {
		Optional<StudentAssignment> entity = studAssignRepo.findByStudentAssignmentKey(studentAssignmentKey);
		StudentAssignment target = null;
		if(entity.isPresent()) {
			target = entity.get();
		}
		return BeanConverter.toStudentAssignmentDto(target);
	}
	
	

}
