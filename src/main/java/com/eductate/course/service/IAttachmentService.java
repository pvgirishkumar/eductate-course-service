package com.eductate.course.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.eductate.course.dto.AttachmentDto;

public interface IAttachmentService {
	
	AttachmentDto createAttachment(MultipartFile mediaFile);
	
	//AttachmentDto updateAttachment(AttachmentDto cateDto);
	
	AttachmentDto getAttachment(String attachmentKey);
	
	List<AttachmentDto> getAllAttachment();

}
