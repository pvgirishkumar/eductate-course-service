package com.eductate.course.service;

import com.eductate.course.dto.StudentAssignmentDto;

public interface IStudentAssignmentService {
	
	StudentAssignmentDto createStudentAssignment(StudentAssignmentDto source);
	
	StudentAssignmentDto updateStudentAssignment(StudentAssignmentDto source);
		
	StudentAssignmentDto getStudentAssignment(String studentAssignmentKey);

}
